<?php


namespace App\Service;

use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BusService
{

    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getData(): Array
    {
        $data = array();

        try {
            $response = $this->httpClient->request('GET',  'http://dadosabertos.rio.rj.gov.br/apiTransporte/apresentacao/rest/index.cfm/obterTodasPosicoes');

            $data = $response->toArray();
        } catch (ClientException $ex) {
            error_log($ex->getMessage());
        }

        return $data;
    }

    public function genereteHashLocation(Array $array){

        $str = implode("", $array);
        return hash('sha1', $str);
    }

}
