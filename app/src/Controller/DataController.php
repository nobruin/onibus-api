<?php

namespace App\Controller;

use App\Entity\Position;
use App\Service\BusService;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends AbstractController
{
    /**
     * @Route("/data", name="data")
     */
    public function index(BusService $busService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Position::class);

        $positions = $busService->getData();
        foreach ($positions['DATA'] as $key => $value) {

                $hash = $busService->genereteHashLocation($value);
                $position = $repository->findOneBy(['hash' => $hash]);
                if(!$position){

                    $date = DateTime::createFromFormat('m-d-Y H:i:s', $value[0]);
                    $position = new Position();
                    $position->setDatahora($date)
                            ->setOrdem($value[1])
                            ->setLatitude($value[3])
                            ->setLongitude($value[4])
                            ->setVelocidade($value[5])
                            ->setHash($hash);

                    $entityManager->persist($position);
                    $entityManager->flush();
                }
        }
        return $this->json($positions);
    }
}
