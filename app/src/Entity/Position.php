<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PositionRepository")
 */
class Position
{

    // public function __construct($datahora , $ordem , $latitude, $longitude, $velocidade )
    // {
    //     $this->datahora =  $datahora;
    //     $this->ordem =  $ordem;
    //     $this->latitude =  $latitude;
    //     $this->longitude =  $longitude;
    //     $this->velocidade = $velocidade;
    // }
    

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datahora;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ordem;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=11, scale=8)
     */
    private $longitude;

    /**
     * @ORM\Column(type="integer")
     */
    private $velocidade;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatahora(): ?\DateTimeInterface
    {
        return $this->datahora;
    }

    public function setDatahora(\DateTimeInterface $datahora): self
    {
        $this->datahora = $datahora;

        return $this;
    }

    public function getordem(): ?string
    {
        return $this->ordem;
    }

    public function setOrdem(string $ordem): self
    {
        $this->ordem = $ordem;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getVelocidade(): ?int
    {
        return $this->velocidade;
    }

    public function setVelocidade(int $velocidade): self
    {
        $this->velocidade = $velocidade;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }
    
    
    public function getInfo() {
        var_dump(get_object_vars($this));
    }
}
